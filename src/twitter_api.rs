use anyhow::bail;
use reqwest::StatusCode;
use rocket::serde::{Deserialize, Serialize};
use serde::de;
use serde_json::Value;

#[derive(Debug, Serialize, Deserialize)]
pub struct TwitterUserInfo {
    id: String,
    name: String,
    username: String,
}

pub async fn get_twitter_by_username(
    username: &str,
    token: &str,
) -> anyhow::Result<TwitterUserInfo> {
    let client = reqwest::Client::new();
    let resp = client
        .get(format!(
            "https://api.twitter.com/2/users/by?usernames={}",
            username
        ))
        .bearer_auth(token)
        .send()
        .await?;

    match resp.status() {
        StatusCode::OK => {
            let mut v: Value = resp.json().await?;
            bail_if_twitter_error(&mut v)?;
            let mut vec_userinfo: Vec<TwitterUserInfo> = take_value_or_bail(&mut v, "data")?;
            // take the first user from userinfo
            Ok(vec_userinfo.remove(0))
        }
        e => {
            bail!(e)
        }
    }
}
pub async fn get_twitter_by_id(id: &str, token: &str) -> anyhow::Result<TwitterUserInfo> {
    let client = reqwest::Client::new();
    let resp = client
        .get(format!("https://api.twitter.com/2/users/{}", id))
        .bearer_auth(token)
        .send()
        .await?;

    match resp.status() {
        StatusCode::OK => {
            let mut v: Value = resp.json().await?;
            bail_if_twitter_error(&mut v)?;
            Ok(take_value_or_bail(&mut v, "data")?)
        }
        e => {
            bail!(e)
        }
    }
}

// twitter return http status of 200 even if there is an error, so need to parse the response to see if there is any `errors` property
fn bail_if_twitter_error(v: &mut Value) -> anyhow::Result<()> {
    if v.get("errors").is_some() {
        let errs = v.get_mut("errors").unwrap();
        let err_detail: String = take_value_or_bail(&mut errs[0], "detail")?;
        bail!(err_detail);
    }
    Ok(())
}

fn take_value_or_bail<T>(v: &mut Value, s: &str) -> anyhow::Result<T>
where
    T: de::DeserializeOwned,
{
    if let Some(x) = v.get_mut(s) {
        Ok(serde_json::from_value(x.take())?)
    } else {
        bail!("Cannot find required json value {}", s)
    }
}
