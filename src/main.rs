#[macro_use]
extern crate rocket;
use rocket::fairing::AdHoc;
use rocket::response::Responder;
use rocket::serde::{json::Json, Deserialize, Serialize};
use rocket::State;

mod twitter_api;

#[derive(Debug, Deserialize)]
#[serde(crate = "rocket::serde")]
struct AppConfig {
    token: String,
}
#[derive(Debug, Serialize, Deserialize)]
struct ResErr {
    message: String,
}

#[derive(Responder)]
enum ResponseJson {
    #[response(status = 200, content_type = "json")]
    Success(Json<twitter_api::TwitterUserInfo>),
    #[response(status = 500, content_type = "json")]
    Error(Json<ResErr>),
}
#[get("/id/<id>")]
// async fn id(id: &str) -> Result<Json<TwitterUserInfo>, status::BadRequest<ResErr>> {
async fn id(id: &str, app_config: &State<AppConfig>) -> ResponseJson {
    match twitter_api::get_twitter_by_id(&id, &app_config.token).await {
        Ok(expr) => ResponseJson::Success(Json(expr)),
        Err(e) => ResponseJson::Error(Json(ResErr {
            message: format!("{}", e),
        })),
    }
}

// #[get("/user/<username>")]
#[derive(Debug, Deserialize, Serialize)]
struct Post {
    username: String,
}
#[post("/user", data = "<post>")]
async fn user(post: Json<Post>, app_config: &State<AppConfig>) -> ResponseJson {
    // post.username.to_owned()
    match twitter_api::get_twitter_by_username(&post.username, &app_config.token).await {
        Ok(expr) => ResponseJson::Success(Json(expr)),
        Err(e) => ResponseJson::Error(Json(ResErr {
            message: format!("{}", e),
        })),
    }
}

#[launch]
fn rocket() -> _ {
    dotenv::dotenv().ok();
    rocket::build()
        .mount("/twitter", routes![id, user])
        .attach(AdHoc::config::<AppConfig>())
}
